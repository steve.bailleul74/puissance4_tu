import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import fr.epsi.puissancek.controller.GameController;
import fr.epsi.puissancek.model.GameModel;
import fr.epsi.puissancek.model.GameMove;
import fr.epsi.puissancek.model.GameMoveList;
import fr.epsi.puissancek.model.PlayerColor;
import fr.epsi.puissancek.model.PlayerType;

class AppTest {
	
	@Test
	void Test_NoEmpty_ListMove_1() {
		
			GameMoveList listemove = new GameMoveList();
			GameMove move = new GameMove(PlayerColor.RED, 3);
			listemove.addMove(move);
			
			
			assertEquals(1, listemove.getSize());
	}
	
	@Test
	public void Test_ValueEqual_Width_Height_Win()
	{
		GameModel model = new GameModel(5, 4, 1);
		assertEquals(5, model.getWidth());	
		assertEquals(4, model.getHeight());	
		assertEquals(1, model.getWinningAlignment());	
	}
	
	@Test
	public void Test_GameModel()
	{
		GameModel model = new GameModel(5, 4, 1);
		int h = model.getColumnHeight(4);
		assertEquals(0, h);
	}
	
	@Test
	public void Test_True_Player()
	{
		GameModel model = new GameModel(5, 4, 2); 
		
		boolean test = model.play(3, PlayerColor.RED);
		assertTrue(test);
	}
	
	
	
	
	/* public void test()
	 {
		GameModel model = new GameModel(5, 4, 2); 
		GameController controle = new GameController(model, redPlayer, yellowPlayer)
		GameController(GameModel model, PlayerType redPlayer, PlayerType yellowPlayer);
	 }*/
	
}
