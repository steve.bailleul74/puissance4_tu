package fr.epsi.puissancek.model;

import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractListModel;

/**
 * Represent a list of game moves.
 */
@SuppressWarnings("serial")
public class GameMoveList extends AbstractListModel<GameMove> {

    private List<GameMove> list = new ArrayList<>();
    
    /**
     * Add a move to the list.
     * 
     * @param move Move to add
     */
    public void addMove(GameMove move) {
    	
        list.add(move);
        fireIntervalAdded(this, getSize() - 1, getSize() - 1);
    }
    
    /**
     * Remove the last move from this list, and return it.
     * 
     * @return Removed element (previous last move)
     */
    public GameMove getAndRemoveLastMove() {
        GameMove move = list.remove(list.size() - 1);
        fireIntervalRemoved(this, getSize(), getSize());
        return move;
    }
    
    /**
     * Get the size of this list.
     * 
     * @return Size of this list
     */
    @Override
    public int getSize() {
        return list.size();
    }
    
    /**
     * Get the move at the given index.
     * 
     * @param index Index to look at
     * @return GameMove at the given position
     */
    @Override
    public GameMove getElementAt(int index) {
        return list.get(index);
    }
}
