package fr.epsi.puissancek.gui;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

import fr.epsi.puissancek.controller.GameController;
import fr.epsi.puissancek.model.GameModel;
import fr.epsi.puissancek.model.PlayerType;

@SuppressWarnings("serial")
public class NewGameSettingsDialog extends JDialog {

    private static final int DEFAULT_WIDTH = 7;
    private static final int DEFAULT_HEIGHT = 6;
    private static final int DEFAULT_MAX_ALIGN = 4;

    private final SpinnerNumberModel widthModel = new SpinnerNumberModel(DEFAULT_WIDTH, 5, 11, 1);
    private final SpinnerNumberModel heightModel = new SpinnerNumberModel(DEFAULT_HEIGHT, 4, 10, 1);
    private final SpinnerNumberModel maxAlignModel = new SpinnerNumberModel(DEFAULT_MAX_ALIGN, 3, 5, 1);
    
    private final JSpinner maxAlign = new JSpinner(maxAlignModel);
    private final JSpinner width = new JSpinner(widthModel);
    private final JSpinner height = new JSpinner(heightModel);
    private final JComboBox<PlayerType> redType = new JComboBox<>(PlayerType.values());
    private final JComboBox<PlayerType> yellowType = new JComboBox<>(PlayerType.values());
    
    private boolean okOption;

    public NewGameSettingsDialog(JFrame owner) {
        super(owner, "Paramètres", true);
        
        getContentPane().add(buildMainPanel());
        
        pack();
        setLocationRelativeTo(owner);
    }
    
    @Override
    public void setVisible(boolean visible) {
        if (visible) {
            okOption = false;
        }
        
        super.setVisible(visible);
    }
    
    public boolean isOkOption() {
        return okOption;
    }
    
    public GameController buildGameController() {
        return new GameController(
                new GameModel((int) widthModel.getNumber(), (int) heightModel.getNumber(), (int) maxAlignModel.getNumber()),
                (PlayerType) redType.getSelectedItem(),
                (PlayerType) yellowType.getSelectedItem()
        );
    }
    
    private JPanel buildMainPanel() {
        JLabel widthLabel = new JLabel("Largeur :");
        JPanel widthPanel = new JPanel();
        widthPanel.add(widthLabel);
        widthPanel.add(width);

        JLabel heightLabel = new JLabel("Hauteur :");
        JPanel heightPanel = new JPanel();
        heightPanel.add(heightLabel);
        heightPanel.add(height);
        
        JLabel alignLabel = new JLabel("Puissance :");
        JPanel alignPanel = new JPanel();
        alignPanel.add(alignLabel);
        alignPanel.add(maxAlign);
        
        JLabel redLabel = new JLabel("Rouge :");
        JPanel redPanel = new JPanel();
        redPanel.add(redLabel);
        redPanel.add(redType);
        
        JLabel yellowLabel = new JLabel("Jaune :");
        JPanel yellowPanel = new JPanel();
        redPanel.add(yellowLabel);
        redPanel.add(yellowType);
        
        JButton confirmButton = new JButton("OK");
        confirmButton.addActionListener((e) -> {
            okOption = true;
            setVisible(false);
        });
        JPanel confirmPanel = new JPanel();
        confirmPanel.add(confirmButton);

        JPanel panel = new JPanel();
        BoxLayout layoutMgr = new BoxLayout(panel, BoxLayout.Y_AXIS);
        panel.setLayout(layoutMgr);
        panel.add(widthPanel);
        panel.add(heightPanel);
        panel.add(alignPanel);
        panel.add(redPanel);
        panel.add(yellowPanel);
        panel.add(confirmPanel);
        
        return panel;
    }
}
