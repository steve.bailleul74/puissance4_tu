package fr.epsi.puissancek.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.event.MouseInputListener;

import fr.epsi.puissancek.controller.GameController;
import fr.epsi.puissancek.model.GameModel;
import fr.epsi.puissancek.model.PlayerColor;

@SuppressWarnings("serial")
public class GamePanel extends JPanel implements MouseInputListener {
    
    public static final int CELL_SIZE_PX = 60;
    
    private static final Color LIGHT_RED = new Color(255, 200, 200);
    private static final Color LIGHT_YELLOW = new Color(255, 255, 200);

    private final Image cellImage;
    
    private GameController controller;
    private GameModel model;
    private int selectedColumn = -1;
    
    public GamePanel() {
        Image img = null;
        try {
            img = ImageIO.read(GamePanel.class.getResourceAsStream("p4_cell.png"));
        } catch (IOException e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "Erreur", JOptionPane.ERROR_MESSAGE);
        }
        cellImage = img;
            
        addMouseListener(this);
        addMouseMotionListener(this);
    }

    public void newGame(GameController controller) {
        this.controller = controller;
        controller.setGamePanel(this);
        this.model = controller.getModel();
        setPreferredSize(new Dimension(CELL_SIZE_PX * model.getWidth(), CELL_SIZE_PX * model.getHeight()));
        repaint();
    }
    
    public GameController getController() {
        return controller;
    }
    
    public GameModel getModel() {
        return model;
    }
    
    public void update() {
        repaint();
        
        if (model.isGameOver()) {
            PlayerColor winner = model.getWinner();
            String message;
            if (null != winner) {
                message = winner + " a gagné !";
            } else {
                message = "Match nul !";
            }
            JOptionPane.showMessageDialog(null, message, "Game over", JOptionPane.INFORMATION_MESSAGE);
        }
    }
    
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        
        if (null != model) {
            paintBoard(g);
            
            if (selectedColumn != -1 && !controller.isGraphicBoardFreezed()) {
                paintPotentialMove(g);
            }
        }
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        if (null != model) {
            selectedColumn = Math.max(0, Math.min(model.getWidth() - 1, e.getX() / CELL_SIZE_PX));
            repaint();
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (!controller.isGraphicBoardFreezed()) {
            controller.play(selectedColumn);
            repaint();
        }
    }

    @Override
    public void mouseExited(MouseEvent e) {
        selectedColumn = -1;
        repaint();
    }
    
    @Override
    public void mouseDragged(MouseEvent e) { }

    @Override
    public void mousePressed(MouseEvent e) { }

    @Override
    public void mouseReleased(MouseEvent e) { }

    @Override
    public void mouseEntered(MouseEvent e) { }
    
    private void paintBoard(Graphics g) {
        for (int y = 0; y < model.getHeight(); ++y) {
            for (int x = 0; x < model.getWidth(); ++x) {
                Color color = Color.white;
                PlayerColor cell = model.getCell(x, y);
                if (null != cell) {
                    if (cell.equals(PlayerColor.RED)) {
                        color = Color.red;
                    } else {
                        color = Color.yellow;
                    }
                }
                g.drawImage(cellImage, x * CELL_SIZE_PX, (model.getHeight() - 1 - y) * CELL_SIZE_PX, CELL_SIZE_PX, CELL_SIZE_PX, color, null);
            }
        }
    }
    
    private void paintPotentialMove(Graphics g) {
        int selectedColumnHeight = model.getColumnHeight(selectedColumn);
        if (selectedColumnHeight < model.getHeight()) {
            Color color;
            if (controller.getNextPlayer().equals(PlayerColor.RED)) {
                color = LIGHT_RED;
            } else {
                color = LIGHT_YELLOW;
            }
            g.drawImage(cellImage, selectedColumn * CELL_SIZE_PX, (model.getHeight() - 1 - selectedColumnHeight) * CELL_SIZE_PX, CELL_SIZE_PX, CELL_SIZE_PX, color, null);
        }
    }
}
