package fr.epsi.puissancek.gui;

import java.awt.BorderLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;

import fr.epsi.puissancek.controller.GameController;
import fr.epsi.puissancek.model.GameMove;

@SuppressWarnings("serial")
public class MovesPanel extends JPanel {

    private JList<GameMove> moveListComponent = new JList<>();
    private final GamePanel gamePanel;
    
    public MovesPanel(GamePanel gamePanel) {
        super(new BorderLayout());
        this.gamePanel = gamePanel;
        
        JButton rewindToButton = new JButton("Revenir au coup");
        rewindToButton.setEnabled(false);
        rewindToButton.addActionListener((e) -> rewindToSelectedMove());
        
        moveListComponent.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        moveListComponent.addListSelectionListener((e) -> {
            if (!e.getValueIsAdjusting()) {
                rewindToButton.setEnabled(!moveListComponent.isSelectionEmpty());
            }
        });
        moveListComponent.addMouseListener(new MouseAdapter() {
            
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    rewindToSelectedMove();
                }
            }
        });
        
        setBorder(BorderFactory.createTitledBorder("Coups joués"));
        add(new JScrollPane(moveListComponent), BorderLayout.CENTER);
        add(rewindToButton, BorderLayout.SOUTH);
    }
    
    public void newGame(GameController controller) {
        moveListComponent.setModel(controller.getMoveList());
    }
    
    private void rewindToSelectedMove() {
        gamePanel.getController().rewindToMove(moveListComponent.getSelectedIndex());
        gamePanel.repaint();
    }
}
