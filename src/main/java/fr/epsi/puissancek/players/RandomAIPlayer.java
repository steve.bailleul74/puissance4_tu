package fr.epsi.puissancek.players;

import java.util.ArrayList;

import fr.epsi.puissancek.controller.GameController;
import fr.epsi.puissancek.model.PlayerColor;

public class RandomAIPlayer extends Player {
    
    private ArrayList<Integer> list = new ArrayList<>();
    
    public RandomAIPlayer(PlayerColor color, GameController controller) {
        super(color, controller);
    }
    
    @Override
    public void run() {
        list.clear();
        for (int i = 0; i < getModel().getWidth(); ++i) {
            if (getModel().getColumnHeight(i) < getModel().getHeight()) {
                list.add(i);
            }
        }
        play(list.get((int) Math.floor(Math.random() * (double) list.size())));
    }

    @Override
    public boolean usesGraphicBoard() {
        return false;
    }
}
