package fr.epsi.puissancek.players;

import fr.epsi.puissancek.controller.GameController;
import fr.epsi.puissancek.model.PlayerColor;

public class HumanPlayer extends Player {

    public HumanPlayer(PlayerColor color, GameController controller) {
        super(color, controller);
    }
    
    @Override
    public void run() {
        
    }

    @Override
    public boolean usesGraphicBoard() {
        return true;
    }
}
